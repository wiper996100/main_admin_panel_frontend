import { combineReducers } from 'redux';
import chatReducer from './chatStore/reducer';
import mainReducer from './mainStore/reducer';
import authReducer from './authStore/reducer';
import modelWindowReducer from './modelWindowStore/reducer';

const rootReducer = combineReducers({
    chatReducer,
    mainReducer,
    authReducer,
    modelWindowReducer
});

export default rootReducer;