const initialState = {
    statusModelWindowPersonnelList: false,
    statusModelWindowCreateAdminStaff: false,
    statusModelWindowChangePassword: false,
    statusModelWindowProfileSetting: false,
    statusModelWindowAdminPanelTheme: false,
    statusModelWindowChat: false,
};

const counterReducer = (state = initialState, action: {
    message: string; type: string; refresh: string, access: string
}) => {
    switch (action.type) {
        case 'OPEN_MODEL_WINDOW_PERSONAL_LIST':
            return { ...state, statusModelWindowPersonnelList: true };
        case 'CLOSE_MODEL_WINDOW_PERSONAL_LIST':
            return { ...state, statusModelWindowPersonnelList: false };
        case 'OPEN_MODEL_WINDOW_CREATE_ADMIN_STAFF':
            return { ...state, statusModelWindowCreateAdminStaff: true };
        case 'CLOSE_MODEL_WINDOW_CREATE_ADMIN_STAFF':
            return { ...state, statusModelWindowCreateAdminStaff: false };
        case 'OPEN_MODEL_WINDOW_CHANGE_PASSWORD':
            return { ...state, statusModelWindowChangePassword: true };
        case 'CLOSE_MODEL_WINDOW_CHANGE_PASSWORD':
            return { ...state, statusModelWindowChangePassword: false };
        case 'OPEN_MODEL_WINDOW_PROFILE_SETTING':
            return { ...state, statusModelWindowProfileSetting: true };
        case 'CLOSE_MODEL_WINDOW_PROFILE_SETTING':
            return { ...state, statusModelWindowProfileSetting: false };
        case 'OPEN_MODEL_WINDOW_ADMIN_PANEL_THEME':
            return { ...state, statusModelWindowAdminPanelTheme: true };
        case 'CLOSE_MODEL_WINDOW_ADMIN_PANEL_THEME':
            return { ...state, statusModelWindowAdminPanelTheme: false };
        case 'OPEN_MODEL_WINDOW_CHAT':
            return { ...state, statusModelWindowChat: true };
        case 'CLOSE_MODEL_WINDOW_CHAT':
            return { ...state, statusModelWindowChat: false };
        default:
            return state;
    }
};

export default counterReducer;
