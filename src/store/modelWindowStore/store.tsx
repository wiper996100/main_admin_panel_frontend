import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import modelWindowReducer from './reducer';
import watchIncrement from './saga';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    modelWindowReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(watchIncrement);

export default store;
