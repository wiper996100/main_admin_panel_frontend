export const openModelWindowsPersonnelList = () => ({ type: 'OPEN_MODEL_WINDOW_PERSONAL_LIST' });
export const closeModelWindowsPersonnelList = () => ({ type: 'CLOSE_MODEL_WINDOW_PERSONAL_LIST' });

export const openModelWindowsCreateAdminStaff = () => ({ type: 'OPEN_MODEL_WINDOW_CREATE_ADMIN_STAFF' });
export const closeModelWindowsCreateAdminStaff = () => ({ type: 'CLOSE_MODEL_WINDOW_CREATE_ADMIN_STAFF' });

export const openModelWindowsChangePassword = () => ({ type: 'OPEN_MODEL_WINDOW_CHANGE_PASSWORD' });
export const closeModelWindowsChangePassword = () => ({ type: 'CLOSE_MODEL_WINDOW_CHANGE_PASSWORD' });

export const openModelWindowsProfileSetting = () => ({ type: 'OPEN_MODEL_WINDOW_PROFILE_SETTING' });
export const closeModelWindowsProfileSetting = () => ({ type: 'CLOSE_MODEL_WINDOW_PROFILE_SETTING' });

export const openModelWindowsAdminPanelTheme = () => ({ type: 'OPEN_MODEL_WINDOW_ADMIN_PANEL_THEME' });
export const closeModelWindowsAdminPanelTheme = () => ({ type: 'CLOSE_MODEL_WINDOW_ADMIN_PANEL_THEME' });

export const openModelWindowsChat = () => ({ type: 'OPEN_MODEL_WINDOW_CHAT' });
export const closeModelWindowsChat = () => ({ type: 'CLOSE_MODEL_WINDOW_CHAT' });
