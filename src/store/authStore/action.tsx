export const POST_DATA = 'POST_DATA';
export const TOKENS = 'TOKENS';
export const POST_DATA_SUCCESS = 'POST_DATA_SUCCESS';
export const POST_DATA_FAILURE = 'POST_DATA_FAILURE';

export const submitData = (payload: { username: string, password: string }) => ({ type: POST_DATA, payload });