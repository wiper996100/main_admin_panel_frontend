import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import authReducer from './reducer';
import watchIncrement from './saga';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    authReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(watchIncrement);

export default store;
