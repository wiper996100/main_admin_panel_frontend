const initialState = {
    isLoader: false,
    isAuth: false,
};

const counterReducer = (state = initialState, action: {
    message: string; type: string; refresh: string, access: string
}) => {
    switch (action.type) {
        case 'TOKENS':
            localStorage.setItem('Refresh-Token', action.refresh);
            localStorage.setItem('Access-Token', action.access);
            return { ...state, isAuth: true };
        case 'POST_DATA_FAILURE':
            return { ...state, isLoader: false, message: action?.message };
        case 'POST_DATA_SUCCESS':
            return { ...state, isLoader: false, message: action?.message };
        case 'POST_DATA':
            return { ...state, isLoader: true };
        default:
            return state;
    }
};

export default counterReducer;
