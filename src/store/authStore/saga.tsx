import axios from "axios";
import { POST_DATA, POST_DATA_FAILURE, POST_DATA_SUCCESS, TOKENS } from "./action";
import { call, put, takeEvery } from "redux-saga/effects";

function* loginSaga(action: { type: string, payload: { username: string, password: string } }) {
    const { payload } = action;
    try {
        // Выполняем POST-запрос с помощью axios
        const response = yield call(axios.post, 'http://localhost:4200/api/login', payload);
        const data = response.data;
        yield put({ type: TOKENS, access: data.access, refresh: data.refresh });
        yield put({ type: POST_DATA_SUCCESS, payload: data, message: data.message, access: data.access, refresh: data.refresh });
    } catch (error: any) {
        yield put({ type: POST_DATA_FAILURE, error: error.message, message: error.response.data.message });
    }
}
// Слушаем действие POST_DATA и выполняем loginSaga при его вызове
function* watchPostData() {
    yield takeEvery(POST_DATA, loginSaga);
}

export default watchPostData;