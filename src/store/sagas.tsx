import { all } from 'redux-saga/effects';
import authSaga from './authStore/saga';

export default function* rootSaga() {
    yield all([
        authSaga()
    ]);
}