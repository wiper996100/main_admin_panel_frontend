import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import mainReducer from './reducer';
import watchIncrement from './saga';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    mainReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(watchIncrement);

export default store;
