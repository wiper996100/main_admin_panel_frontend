import { ReactNode } from "react"

export interface InitialState {
    statusModelWindowPersonnelList: boolean,
    statusModelWindowCreateAdminStaff: boolean,
    statusModelWindowChangePassword: boolean,
    statusModelWindowProfileSetting: boolean,
    statusModelWindowAdminPanelTheme: boolean,
    isLoader: boolean,
    isAuth: boolean,
    message: string,
    counterMessage: number
}

export interface InitialStateMemo {
    data: {
        statusModelWindowPersonnelList: boolean,
        statusModelWindowCreateAdminStaff: boolean,
        statusModelWindowChangePassword: boolean,
        statusModelWindowProfileSetting: boolean,
        statusModelWindowAdminPanelTheme: boolean,
        isLoader: boolean,
        isAuth: boolean,
        message: string,
        counterMessage: number
    }
}

export interface onClick {
    onClick: () => void
}

export interface PropsAppsMenu {
    icon: ReactNode,
    type: string,
    children: ReactNode
}


export interface PropsModalCanvas {
    open: boolean,
    close?: () => { type: string } | null,
    children: ReactNode,
    cross?: boolean
}