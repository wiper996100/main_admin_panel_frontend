import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { List, ModalClose } from '@mui/joy';
import ModalCanvas from '@/components/ModalCanvas';
import {
    closeModelWindowsPersonnelList,
    closeModelWindowsCreateAdminStaff,
    closeModelWindowsProfileSetting,
    closeModelWindowsChangePassword,
    closeModelWindowsAdminPanelTheme,
    closeModelWindowsChat,
} from '@/store/modelWindowStore/action';
import Chat from '@/components/Chat';

function Home() {
    const dispatch = useDispatch()
    const {
        statusModelWindowAdminPanelTheme,
        statusModelWindowChangePassword,
        statusModelWindowChat,
        statusModelWindowCreateAdminStaff,
        statusModelWindowPersonnelList,
        statusModelWindowProfileSetting
    }: {
        statusModelWindowAdminPanelTheme: boolean,
        statusModelWindowChangePassword: boolean,
        statusModelWindowChat: boolean,
        statusModelWindowCreateAdminStaff: boolean,
        statusModelWindowPersonnelList: boolean,
        statusModelWindowProfileSetting: boolean
    } = useSelector(({ modelWindowReducer }) => modelWindowReducer);
    return (
        <>
            <ModalCanvas open={statusModelWindowChat} cross close={() => dispatch(closeModelWindowsChat())}>
                <Chat />
            </ModalCanvas>
            <ModalCanvas open={statusModelWindowPersonnelList} cross close={() => dispatch(closeModelWindowsPersonnelList())}>
                model1
            </ModalCanvas>
            <ModalCanvas open={statusModelWindowCreateAdminStaff} cross close={() => dispatch(closeModelWindowsCreateAdminStaff())}>
                model2
            </ModalCanvas>
            <ModalCanvas open={statusModelWindowChangePassword} cross close={() => dispatch(closeModelWindowsChangePassword())}>
                model3
            </ModalCanvas>
            <ModalCanvas open={statusModelWindowProfileSetting} cross close={() => dispatch(closeModelWindowsProfileSetting())}>
                model4
            </ModalCanvas>
            <ModalCanvas open={statusModelWindowAdminPanelTheme} cross close={() => dispatch(closeModelWindowsAdminPanelTheme())}>
                model5
            </ModalCanvas>
        </>
    );
}

export default Home;
