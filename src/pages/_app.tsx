import type { AppProps } from 'next/app'
import { Provider } from 'react-redux';
import ButtonMessages from '@/components/Buttons/ButtonMessage';
import Menu from '@/components/Menu';
import storeModelWindowsState from '@/store/store';
import storeChatState from '@/store/chatStore/store';
import RedirectToLogin from '@/utiles/redirects';
import { Box } from '@mui/joy';
import '@/styles/global.css';

export default function App({ Component, pageProps }: AppProps) {
    return (
        <Provider store={storeModelWindowsState}>
            <RedirectToLogin />
            <Box style={{ display: 'flex' }}>
                <Menu />
                <Box style={{ width: '97%', backgroundColor: '#d9d9e3' }}>
                    <Component {...pageProps} />
                    <ButtonMessages />
                </Box>
            </Box>
        </Provider>
    )
}