import React, { useEffect } from 'react'
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import LoginForm from '@/components/Forms/login.form';
import ModalCanvas from '@/components/ModalCanvas'
import Loading from '@/components/Loading';
import { memoizedSelectorLogin } from '@/utiles/memo/memoizedSelectorLogin';

function Login() {
    const router = useRouter();
    const { data: { isAuth, isLoader } } = useSelector(memoizedSelectorLogin);


    useEffect(() => {
        if (isAuth) {
            router.push('/');
        }
    }, [isAuth]);
    return (
        <ModalCanvas open>
            <LoginForm />
            <Loading isLoader={isLoader} />
        </ModalCanvas>
    )
}

export default Login