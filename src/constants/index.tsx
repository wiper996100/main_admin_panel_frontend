import {
    openModelWindowsPersonnelList,
    openModelWindowsCreateAdminStaff,
    openModelWindowsChangePassword,
    openModelWindowsProfileSetting,
    openModelWindowsAdminPanelTheme,
} from "@/store/mainStore/action";

export const ADMINISTRATIVE_STAFF_MENU_BUTTON = [{
    name: 'Список персонала',
    avatar: 'S',
    openMethod: openModelWindowsPersonnelList(),
    style: { textAlign: "center", fontSize: '10px' }
},
{
    name: 'Создать административный персонал',
    avatar: 'M',
    openMethod: openModelWindowsCreateAdminStaff(),
    style: { textAlign: "center", fontSize: '10px' }
}]

export const CLIENTS_MENU = [{
    name: 'Список клиентов',
    avatar: 'S',
    openMethod: openModelWindowsPersonnelList(),
    style: { textAlign: "center", fontSize: '10px' }
},
{
    name: 'Создать клиента',
    avatar: 'M',
    openMethod: openModelWindowsCreateAdminStaff(),
    style: { textAlign: "center", fontSize: '10px' }
}]

export const SETTINGS_MENU = [{
    name: 'Сменить пароль',
    avatar: 'S',
    openMethod: openModelWindowsChangePassword(),
    style: { textAlign: "center", fontSize: '10px' }
},
{
    name: 'Настройка профиля',
    avatar: 'M',
    openMethod: openModelWindowsProfileSetting(),
    style: { textAlign: "center", fontSize: '10px' }
},
{
    name: 'Тема панели администратора',
    avatar: 'S',
    openMethod: openModelWindowsAdminPanelTheme(),
    style: { textAlign: "center", fontSize: '10px' }
},]
