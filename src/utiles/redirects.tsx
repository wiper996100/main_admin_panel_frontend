import { useEffect } from 'react';
import { useRouter } from 'next/router';

const RedirectToLogin = (): void => {
    const router = useRouter();
    useEffect(() => {
        const userIsLoggedIn = localStorage.getItem('Access-Token');
        if (!userIsLoggedIn) {
            router.push('/login');
        }
    }, []);
};

export default RedirectToLogin;