function formLoginValidator(values: { username: string, password: string }) {
    const errors: { username: string, password: string } = {
        username: "",
        password: ""
    }
    if (!values.username) {
        errors.username = 'Required'
    }
    if (!values.password) {
        errors.password = 'Required'
    }
    // return errors
}

export default formLoginValidator