import { createSelector } from 'reselect';

export const memoizedSelectorStatusModelWindow = createSelector(
    ({ statusModelWindowPersonnelList,
        statusModelWindowCreateAdminStaff,
        statusModelWindowChangePassword,
        statusModelWindowProfileSetting,
        statusModelWindowAdminPanelTheme,
        statusModelWindowChat, a }): {
            statusModelWindowPersonnelList: boolean,
            statusModelWindowCreateAdminStaff: boolean,
            statusModelWindowChangePassword: boolean,
            statusModelWindowProfileSetting: boolean,
            statusModelWindowAdminPanelTheme: boolean,
            statusModelWindowChat: boolean,
            a: string
        } => {
        return {
            statusModelWindowPersonnelList,
            statusModelWindowCreateAdminStaff,
            statusModelWindowChangePassword,
            statusModelWindowProfileSetting,
            statusModelWindowAdminPanelTheme,
            statusModelWindowChat,
            a
        }
    },
    (data) => ({ data })
);
console.log(memoizedSelectorStatusModelWindow);

export const memoizedSelectorMessageModelWindow = createSelector(
    ({ message }): {
        message: string,
    } => {
        return {
            message
        }
    },
    (data) => ({ data })
);