import { createSelector } from 'reselect';

export const memoizedSelectorLogin = createSelector(
    ({ isLoader, isAuth }): { isLoader: boolean, isAuth: boolean } => {
        return {
            isLoader,
            isAuth
        }
    },
    (data) => ({ data })
);