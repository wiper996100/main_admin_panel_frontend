import React from 'react'
import { LinearProgress } from '@mui/joy';

function Loading({ isLoader }: { isLoader: boolean }) {
    return (
        <>
            {isLoader && <LinearProgress thickness={1} />}
        </>
    )
}

export default Loading