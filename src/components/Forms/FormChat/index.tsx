import formLoginValidator from '@/utiles/formValidators/login.validator'
import { Send } from '@mui/icons-material'
import { Box, IconButton, Textarea } from '@mui/joy'
import React from 'react'
import { Field, Form } from 'react-final-form'
import MessageTextField from '../FormElements/MessageTextField.FormElement'

function FormChat({ submitMessage }) {
    return (
        <Form
            onSubmit={submitMessage}
            validate={formLoginValidator}
            render={({ handleSubmit }) => (
                <Box sx={{ display: 'flex', justifyContent: 'space-around' }}>
                    <form onSubmit={handleSubmit} style={{ width: '100%', display: 'flex' }}>
                        <Field name="message" placeholder="Отправить сообщение..." variant="outlined" component={MessageTextField} />
                        <IconButton type="submit" variant="plain">
                            <Send />
                        </IconButton>
                    </form>
                </Box>
            )}
        />

    )
}

export default FormChat