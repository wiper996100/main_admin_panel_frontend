import React, { ReactElement } from 'react'
import InputLib from '@mui/joy/Input';
import { Textarea } from '@mui/joy';


function MessageTextField(props: { input: ReactElement; placeholder: string; }) {
    const { input, placeholder } = props
    return (
        <Textarea {...input} placeholder={placeholder} sx={{ maxHeight: '64px', width: '95%' }} />
    )
}

export default MessageTextField