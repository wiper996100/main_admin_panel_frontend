import React, { ReactElement } from 'react'
import InputLib from '@mui/joy/Input';


function Input(props: { input: ReactElement; placeholder: string; }) {
    const { input, placeholder } = props
    return (
        <InputLib {...input} placeholder={placeholder} />
    )
}

export default Input