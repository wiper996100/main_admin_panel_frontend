import React from 'react'
import { useDispatch } from 'react-redux'
import { Form, Field } from 'react-final-form'
import { Box, Button, Typography } from '@mui/joy'
import Input from '@/components/Forms/FormElements/TextField.FormElement'
import formLoginValidator from '@/utiles/formValidators/login.validator'
import { submitData } from '@/store/mainStore/actions'

function LoginForm() {
    const dispatch = useDispatch();
    const onSubmit = async (values: { username: string, password: string }) => {
        dispatch(submitData(values))

    }
    return (
        <Form
            onSubmit={onSubmit}
            validate={formLoginValidator}
            render={({ handleSubmit, form, submitting, pristine, values }) => (
                <form onSubmit={handleSubmit}>
                    <Box style={{ margin: '20px' }}>
                        <Typography level="title-lg">Вход в панель администратора</Typography>
                    </Box>
                    <Box style={{ margin: '10px' }}>
                        <Field name="username" placeholder="Имя пользователя" type='text' component={Input} />
                    </Box>
                    <Box style={{ margin: '10px' }}>
                        <Field name="password" placeholder="Пароль" type='password' component={Input} />
                    </Box>
                    <Box style={{ margin: '10px' }}>
                        <Button size="md" variant={'solid'} disabled={submitting} color="neutral" type='submit'>
                            Войти
                        </Button>
                    </Box>
                </form>
            )}
        />
    )
}

export default LoginForm