import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, IconButton, Badge } from '@mui/joy';

import { openModelWindowsChat } from '@/store/mainStore/action';
import { InitialState } from '@/interfases';
import MailIcon from '@mui/icons-material/Mail';


export default function ButtonMessages() {
    const counterMessage = useSelector((store: InitialState) => store.counterMessage);
    const dispatch = useDispatch()
    return (
        <Box sx={{ position: 'absolute', right: '30px', bottom: '30px' }}>
            <IconButton onClick={() => dispatch(openModelWindowsChat())}>
                <Badge badgeContent={counterMessage} badgeInset="1%">
                    <MailIcon sx={{ width: '40px', height: '40px' }} />
                </Badge>
            </IconButton>
        </Box>
    );
}