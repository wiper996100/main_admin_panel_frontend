import * as React from "react";
import * as ReactDOM from "react-dom/client";
import { StyledEngineProvider, CssVarsProvider } from "@mui/joy/styles";
import CssBaseline from "@mui/joy/CssBaseline";

ReactDOM.createRoot(document.querySelector("#root")).render(
    <React.StrictMode>
        <StyledEngineProvider injectFirst>
            <CssVarsProvider>
                <CssBaseline />
                asdasdas
            </CssVarsProvider>
        </StyledEngineProvider>
    </React.StrictMode>
);
