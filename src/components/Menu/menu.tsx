import React, { ReactNode } from 'react';
import { MenuButton, Dropdown, IconButton } from '@mui/joy';
import { PropsAppsMenu } from '@/interfases';

export default function AppsMenu({ icon, type = "button", children }: PropsAppsMenu) {
    return (
        <Dropdown>
            <MenuButton
                slots={{ root: IconButton }}
                slotProps={{ root: { variant: 'plain', color: 'neutral' } }}
                sx={{ borderRadius: 10, color: 'hsla(0,0%,100%,.7)' }}
            >
                {icon}
            </MenuButton>
            {children}
        </Dropdown>
    );
}