import React from 'react'
import { useDispatch } from 'react-redux';
import { Avatar, ListItemDecorator, Menu, MenuItem } from '@mui/joy';
import { CLIENTS_MENU } from '@/constants';

function ClientsMenu() {
    const dispatch = useDispatch()
    return (
        <Menu
            variant="solid"
            invertedColors
            aria-labelledby="apps-menu-demo"
            sx={{
                '--List-padding': '0.5rem',
                '--ListItemDecorator-size': '3rem',
                display: 'grid',
                gridTemplateColumns: 'repeat(3, 100px)',
                gridAutoRows: '100px',
                gap: 1,
            }}
        >
            {CLIENTS_MENU.map(({ name, avatar, openMethod, style }) => (
                <MenuItem key={name} onClick={() => dispatch(openMethod)} orientation="vertical" style={style}>
                    <ListItemDecorator>
                        <Avatar>{avatar}</Avatar>
                    </ListItemDecorator>
                    {name}
                </MenuItem>
            ))}
        </Menu>
    )
}

export default ClientsMenu