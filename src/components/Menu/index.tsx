import * as React from 'react';
import { Settings, Engineering, People } from '@mui/icons-material';
import { Box, Menu as MenuLib } from '@mui/joy';
import AdministrativeStaffMenu from '@/components/Menu/Submenu/AdministrativeStaffMenu';
import SettingsMenu from '@/components/Menu/Submenu/SettingsMenu';
import ClientsMenu from '@/components/Menu/Submenu/ClientsMenu';
import AppsMenu from '@/components/Menu/menu';

export default function Menu() {
    return (
        <Box style={{
            width: '3%',
            display: 'flex',
            justifyContent: 'center',
            flexWrap: 'wrap',
            backgroundColor: 'rgba(32,33,35)',
            textAlign: 'center',
            height: '100vh',
            minWidth: '50px'

        }}>
            <Box style={{ paddingTop: '10px', }}>
                <AppsMenu type="menu" icon={<Engineering />}>
                    <AdministrativeStaffMenu />
                </AppsMenu>
                <AppsMenu type="menu" icon={<People />}>
                    <ClientsMenu />
                </AppsMenu>
            </Box>
            <Box style={{ paddingTop: '10px', paddingBottom: '10px', display: 'flex', flexWrap: 'wrap', alignItems: 'flex-end' }}>
                <AppsMenu type="menu" icon={<Settings />}>
                    <SettingsMenu />
                </AppsMenu>
            </Box>
        </Box>
    );
}