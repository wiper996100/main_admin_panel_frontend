import React, { ReactNode } from 'react';
import { Modal, ModalClose, Sheet } from '@mui/joy';
import { PropsModalCanvas } from '@/interfases';


function ModalCanvas({ children, open = true, cross = false, close = () => null }: PropsModalCanvas) {
    return (
        <React.Fragment>
            <Modal
                aria-labelledby="modal-title"
                aria-describedby="modal-desc"
                open={open}
                onClose={close}
                sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
            >
                <Sheet
                    variant="outlined"
                    sx={{
                        width: '60%',
                        borderRadius: 'md',
                        p: 3,
                        boxShadow: 'lg',
                    }}
                >
                    {cross && <ModalClose
                        variant="outlined"
                        sx={{
                            top: 'calc(-1/4 * var(--IconButton-size))',
                            right: 'calc(-1/4 * var(--IconButton-size))',
                            boxShadow: '0 2px 12px 0 rgba(0 0 0 / 0.2)',
                            borderRadius: '50%',
                            bgcolor: 'background.surface',
                        }}
                    />}
                    {children}
                </Sheet>
            </Modal>
        </React.Fragment>
    );
}
export default ModalCanvas;