import React, { useEffect, useState } from 'react'
import { Box } from '@mui/joy'
import Messages from './Messages'
import FormChat from '../Forms/FormChat'
import { io } from 'socket.io-client'

function InputChat() {
    const [statusChat, setStatusChat] = useState(false)
    const socket = io('http://localhost:5400')
    useEffect(() => {
        socketInitializer()
    }, [])

    const socketInitializer = async () => {
        socket.on('connect', () => {
            if (socket.connected) {
                setStatusChat(true)
            } else {
                setStatusChat(false)
            }
        })
    }
    const submitMessage = (value) => {
        console.log(value)

        // socket.emit('hello', value)
    }
    // socket.on('hello', (data) => {
    //     console.log('asaaaa')
    // })
    return (
        <Box sx={{ width: '100%', padding: '10px', height: '67vh' }}>
            {statusChat ? 'Онлайн' : 'Оффлайн'}
            <Messages />
            <FormChat submitMessage={submitMessage} />
        </Box>
    )
}

export default InputChat