import { Alert, Box } from '@mui/joy'
import React from 'react'

function Messages() {
    return (
        <Box sx={{ height: '88%', overflow: 'auto', marginBottom: '20px' }}>
            <Box sx={{ width: '100%', display: 'flex', justifyContent: 'right', marginBottom: '10px' }}>
                <Alert variant="solid" color="primary" sx={{ width: '50%' }}> asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas</Alert>
            </Box>
            <Box sx={{ width: '100%', display: 'flex', justifyContent: 'left', marginBottom: '10px' }}>
                <Alert variant="solid" color="success" sx={{ width: '50%' }}>asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas asdasdas</Alert>
            </Box>
        </Box>
    )
}

export default Messages