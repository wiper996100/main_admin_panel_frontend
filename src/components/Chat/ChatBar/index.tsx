import { Avatar, Box, List, ListDivider, ListItem, ListItemDecorator } from '@mui/joy'
import React from 'react'

function ChatBar() {
    return (

        <Box>
            <List
                variant="outlined"
                sx={{
                    minWidth: 240,
                    minHeight: 660,
                    borderRadius: 'sm',
                    maxHeight: '700px',
                    overflow: 'auto'
                }}
            >
                <ListItem>
                    <ListItemDecorator>
                        <Avatar size="sm" src="/static/images/avatar/1.jpg" />
                    </ListItemDecorator>
                    Mabel Boyle
                </ListItem>
                <ListDivider inset={'startDecorator'} />
            </List>
        </Box>
    )
}

export default ChatBar