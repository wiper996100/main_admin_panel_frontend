import React, { useEffect } from 'react'
import { Box, List } from '@mui/joy'
import ChatBar from './ChatBar'
import InputChat from './inputChat'

function Chat() {

    return (
        <Box>
            <List
                variant="outlined"
                sx={{
                    minWidth: 240,
                    borderRadius: 'sm',
                    padding: '0px',
                    display: 'flex',
                    flexDirection: 'row'
                }}
            >
                <ChatBar />
                <InputChat />
            </List>
        </Box>
    )
}

export default Chat